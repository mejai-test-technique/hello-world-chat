# Hello World Chat

## Description

Il s'agit d'une page html permettant aux utilisateurs de s'envoyer des messages.<br/>
La page est réalisée en utilisant uniquement le framework Angular sans code serveur.<br/>
![Aperçu de l'écran d'échanges de messages dans une chat room](https://gitlab.com/mejai-test-technique/hello-world-chat/-/raw/main/source-code/src/assets/screenshots/hello-chat-screenshot-1.png)

## Installation

Pour récupérer la page html et le code source, il suffit de cloner le dépôt en utilisant cette commande :<br/>

```
git clone https://gitlab.com/mejai-test-technique/hello-world-chat.git
```

Ensuite, ouvrir la page **index.html** sous le répertoire **application** pour lancer la chat room.<br/>
Le code source **Angular** de l'application est sous le répertoire **source-code**.<br/>

## Choix techniques

L'application est composée d'un composant principal **app** qui instancie le composant **chat-room**.<br/>
Ce dernier accèpte un paramètre en input de type **ChatRoom**.<br/>
Une ChatRoom est définie par un identifiant (**Id**) et deux partenaires de type **ChatPartner**.<br/>
Un ChatPartner est défini par un identifiant (**Id**) et un nom (**Name**).<br/>

Le composant chat-room instancie deux composants de type **chat-window**.<br/>
Ce composant a besoin de trois paramètres en input pour son fonctionnement :<br/>
1. **ChatRoomId** : Identifiant de la chat room dans laquelle se déroule la discussion.<br/>
2. **SourcePartner** : de type ChatPartner qui réprésente le partenaire propriétaire de la fenêtre de discussion.<br/>
3. **TargetParnter** : de type ChatPartner qui représente le partenaire échangeant avec le partenaire propriétaire de la fenêtre de discussion.<br/>

Les deux partenaires échangent des messages via un objet de type **ChatMessage**.<br/>
Un message est défini par :<br/>
1. **ChatRoomId** : Identifiant de la chat room<br/>
2. **SourcePartnerId** : Identifiant du partenaire envoyant le message<br/>
3. **TargetPartnerId** : Identifiant du partenaire recevant le message<br/>
4. **Message** : Le contenu du message<br/>
5. **Date** : La date d'envoi du message<br/>

**Deux** scénarios sont gérés par le composant chat-window : <br/>
1. Envoi des messages :<br/>
L'envoi d'un message se fait suite au click sur le bouton Envoyer.<br/>
C'est le service **ChatRoomService** qui assure le transfert du message (de type **ChatMessage**) vers le destinataire.<br/>
Le conteneur des messages est un objet de type **ReplaySubject**.<br/>
Ce qui permet au partenaire de recevoir les messages envoyés avant son abonnement à l'observateur **Messages**.
2. Réception des messages :<br/>
La réception des messages se fait en s'abonnant à l'observateur **Messages** exposé par le service ChatRoomService.<br/>
Le composant chat window devrait afficher uniquement les messages de la même chat room et ceux échangés entre les deux partenaires.

