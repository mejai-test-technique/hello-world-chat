//Classe définissant un message dans le chat room
export class ChatMessage {
  //Identifiant de la chat room
  ChatRoomId?: number;

  //Identifiant du l'expéditeur du message
  SourcePartnerId?: number;

  //Identifiant du destinatire du message
  TargetPartnerId?: number;

  //Contenu du message
  Message?: string;

  //Date du message
  Date: Date = new Date();
}
