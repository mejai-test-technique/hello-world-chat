//Classe définissant un partenaire dans une chat room
export class ChatPartner {
  //Identifiant du partenaire
  Id: number = 0;

  //Nom du partenaire
  Name: string = '';
}
