import { ChatPartner } from "./chat-partner.model";

//Classe définissant une chat room
export class ChatRoom {
  //Identifiant de la chat room
  Id: number = 0;

  //Partenaire A
  PartnerA: ChatPartner = new ChatPartner();

  //Partenaire B
  PartnerB: ChatPartner = new ChatPartner();
}
