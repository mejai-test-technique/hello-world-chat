import { Injectable } from '@angular/core';
import { ɵangular_packages_router_router_b } from '@angular/router';
import { Observable, ReplaySubject } from 'rxjs';
import { ChatMessage } from 'src/models/chat-message.model';

@Injectable({
  providedIn: 'root'
})
export class ChatRoomService {
  private _MessagesSubject = new ReplaySubject<ChatMessage>();
  public readonly Messages: Observable<ChatMessage> = this._MessagesSubject.asObservable();

  //Envoyer le message vers le destinatire
  SendMessage(message: ChatMessage) {
    if (this.IsValidMessage(message)) {
      this._MessagesSubject.next(message);
    }
  }

  //Vérifier que le message est valide
  private IsValidMessage(message: ChatMessage): boolean {
      if (message && message.ChatRoomId && message.SourcePartnerId && message.TargetPartnerId && message.Message && message.Message?.trim() !== '')
        return true;
      else return false;
  }
}
