import { DatePipe } from '@angular/common';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ChatMessage } from 'src/models/chat-message.model';
import { ChatPartner } from 'src/models/chat-partner.model';
import { ChatRoomService } from 'src/services/chat-room.service';

@Component({
  selector: 'chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.css'],
  providers: [DatePipe]
})
export class ChatWindowComponent implements OnInit, OnDestroy {
  //Identifiant de la room de chat
  @Input() ChatRoomId?: number;

  //Le partenaire source
  @Input() SourcePartner?: ChatPartner;

  //Le partenaire destinataire
  @Input() TargetParnter?: ChatPartner;

  chatWindowForm = new FormGroup({
    message: new FormControl('')
  });

  //Liste des messages échangés entre les deux partenaires
  Messages: Array<ChatMessage> = [];

  private _MessagesSubscription : Subscription = Subscription.EMPTY;

  constructor(private chatRoomService: ChatRoomService, public datepipe: DatePipe) { }

  ngOnInit() {
    //Abonnement à l'observable pour récupérer les messages
    //Afficher que les messages dont il est destinataire ou expéditeur
    this._MessagesSubscription = this.chatRoomService.Messages
    .subscribe((message: ChatMessage) => {
      if (  message.ChatRoomId === this.ChatRoomId
        && (message.TargetPartnerId === this.SourcePartner?.Id || message.SourcePartnerId === this.SourcePartner?.Id)) {
        this.Messages.push(message);
      }
    });
  }

  //Renvoyer le contrôle input texte contenant le message
  get MessageControl(): AbstractControl {
    return this.chatWindowForm.get('message')!;
  }

  //Envoyer le message vers le destinataire en faisant appel au service chat
  SendMessage() {
    if (this.MessageControl.value && this.MessageControl.value.trim() != '') {
      const message = new ChatMessage();
      message.ChatRoomId = this.ChatRoomId!;
      message.SourcePartnerId = this.SourcePartner?.Id!;
      message.TargetPartnerId = this.TargetParnter?.Id!;
      message.Message = this.MessageControl.value.trim();
      this.MessageControl.setValue('');
      this.chatRoomService.SendMessage(message);
    }
  }

  ngOnDestroy() {
    //Pour des questions de performances et pour éviter tout effet de bord
    if (this._MessagesSubscription) {
      this._MessagesSubscription.unsubscribe();
    }
  }
}
