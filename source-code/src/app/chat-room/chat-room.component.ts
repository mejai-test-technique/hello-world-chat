import { Component, Input } from '@angular/core';
import { ChatRoom } from 'src/models/chat-room.model';

@Component({
  selector: 'chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.css']
})
export class ChatRoomComponent {
  //Définition de la chat room
  @Input() ChatRoom?: ChatRoom;
}
