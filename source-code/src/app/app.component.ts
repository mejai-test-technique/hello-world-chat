import { Component, OnInit } from '@angular/core';
import { ChatPartner } from 'src/models/chat-partner.model';
import { ChatRoom } from 'src/models/chat-room.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  //Définition de la chat room
  ChatRoom: ChatRoom = new ChatRoom();

  ngOnInit(): void {
    //Partenaire A de la chat room
    const chatPartnerA = new ChatPartner();
    chatPartnerA.Id = 1;
    chatPartnerA.Name = 'Chat A';

    //Partenaire B de la chat room
    const chatPartnerB = new ChatPartner();
    chatPartnerB.Id = 2;
    chatPartnerB.Name = 'Chat B';

    this.ChatRoom.Id = 1;
    this.ChatRoom.PartnerA = chatPartnerA;
    this.ChatRoom.PartnerB = chatPartnerB;
  }
}
